﻿using System.Collections;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    #region Singleton
    public static AudioManager Instance { get; private set; }

    private void Awake() {

        if (Instance != null && Instance != this) {

            Destroy(gameObject);

        } else {

            Instance = this;
        }
    }
    #endregion

    [SerializeField] private AudioClip start;
    [SerializeField] private AudioClip forward;
    [SerializeField] private AudioClip reverse;
    [SerializeField] private AudioClip brake;
    [SerializeField] private AudioClip stopped;

    public AudioSource AudioSource { get; private set; }

    private void Start() {

        AudioSource = GetComponent<AudioSource>();
    }

    public void SwitchAudio(int audioIndex) {

        switch(audioIndex) {

            case 0:

                if (AudioSource.clip == start && 
                    AudioSource.time >= 3.5f ||
                    AudioSource.clip == forward) {

                    AudioSource.loop = true;
                    PlayAudioClip(true, 3.3f, forward.length, forward);

                } else if (AudioSource.clip != forward) {

                    PlayAudioClip(false, 0, start.length, start);
                }

                break;

            case 1:

                if (AudioSource.clip == start &&
                    AudioSource.time >= 3.5f ||
                    AudioSource.clip == reverse) {

                    AudioSource.loop = true;
                    PlayAudioClip(true, 3.3f, reverse.length, reverse);

                } else if (AudioSource.clip != reverse) {

                    PlayAudioClip(false, 0, start.length, start);
                }

                break;

            case 2:

                AudioSource.loop = false;
                PlayAudioClip(false, 0.44f, brake.length,brake);

                break;

            case 3:
                AudioSource.loop = true;
                PlayAudioClip(true, 2.41f, 2.62f, stopped);
                break;
        }
    }

    private void PlayAudioClip(bool loop, float startTime, float endTime, AudioClip clip) {

        if (AudioSource.clip == clip && !loop) return;

        if (AudioSource.clip != clip || AudioSource.time >= endTime) {

            AudioSource.clip = clip;
            AudioSource.time = startTime;
            AudioSource.Play();
        }

        AudioSource.SetScheduledEndTime(AudioSettings.dspTime + (endTime - startTime));
    }
}
